This directory contains modified context switching code from the Boost library.

It is licensed under the Boost Software License, with the license text in this directory.
